;; TITLE: EasyLog sensor script
;; DESCRIPTION: Simulates launch/read actions for Lascar CO sensor (see readme for setup instructions)
;; AUTHOR: Kiel Gilleade
;; DATE: 8/28/2020

HoldForWindow(dialogTitle, attempts, sleepDuration)
{
	count:= 0
	while(true){
		If WinExist(dialogTitle){
			WinActivate
			return true
		}
		else {
			count++
			Sleep sleepDuration
		}
	
		if (count >= attempts ){
			MsgBox, Failed to load '%dialogTitle%'
			return false
		}
	}
}

Init(windowTitle, path)
{
	If WinExist(windowTitle)
		WinActivate
	else
		run %path% 

	if(!HoldForWindow(windowTitle, 200, 10))	;; Requires more than 1 second to boot
		return false
	
	return true
}

Launch_Lascar(filename){
		
	Send {Enter}	; Menu  
	
	Sleep 1000							; Optional dialog 
	If WinExist("Possible Data Loss")
		Send {Enter}
	
	Sleep 100
	Send {tab}		; Saftey agreement
	Send {space}
	Send {tab}
	Send {tab}
	Send {tab}
	Send {Enter}
		
	Sleep 100
	Send {tab}		; Session name and sample frequency
	Send %filename%	
	Send {tab}		; Default sample frequency (last known)
	Send {tab}
	Send {tab}
	Send {tab}
	Send {Enter}

	Sleep 100
	Send {Enter}	; Warning configuration
	
	Sleep 100
	Send {tab}		; Start configuration
	Send {tab}
	Send {tab}
	Send {tab}
	Send {Enter}

	Sleep 1000
	Send {Enter}	; Start
	
	return true
}

GetData_Lascar()
{
	Send {tab}
	Send {Enter}
	Sleep 1000
	If WinExist("Are you sure?")		; Occurs only when the device is running 
		Send {Enter}
	Send {Enter}
	
	Sleep 1000			; Wait for save-as dialog
	Send {Enter}
	Sleep 1000			; Wait for overwrite file dialog
	If WinExist("Confirm Save As"){		; Only if file already exists 
		Send {tab}
		Send {Enter}
		}
		
	if(!HoldForWindow("EasyLogGraph", 40, 500)) ; 20 seconds
		return false
	Sleep 1000
	Send !{F4}	
	return true
}

^j::
counter:= 0
FormatTime, prefix, ,yyMMdd_hhmmss
path:= "C:\Program Files (x86)\EasyLog USB\EasyLog USB.exe"

while (counter < 5){
	
	if(!Init("EasyLogUSB", path))
		return
	
	filename:= prefix . counter
	
	if(!Launch_Lascar(filename))
		return
		
	Send !{F4}	
	Sleep 60000
	
	if(!Init("EasyLogUSB", path))
		return
	
	if(!GetData_Lascar())
		return
		
	Sleep 500
	Send !{F4}	
	Sleep 500
	counter++
}

MsgBox, Simulation complete
return