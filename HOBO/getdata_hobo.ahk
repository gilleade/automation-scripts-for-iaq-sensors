;; TITLE: HOBOware sensor script
;; DESCRIPTION: Simulates read action for HOBOware sensor (see readme for setup instructions)
;; AUTHOR: Kiel Gilleade
;; DATE: 8/3/2020

HoldForWindow(dialogTitle, attempts, sleepDuration)
{
	count:= 0
	while(true){
		If WinExist(dialogTitle){
			WinActivate
			return true
		}
		else {
			count++
			Sleep sleepDuration
		}
	
		if (count >= attempts ){
			MsgBox, Failed to load %dialogTitle%
			return false
		}
	}
}

^j::
filename:=dummydata

If WinExist("HOBOware")
	WinActivate
else
	run D:\Program Files\Onset Computer Corporation\HOBOware\HOBOware.exe


if(!HoldForWindow("HOBOware", 200, 10))	;; Requires more than 1 second to boot
	return

Send ^r	; Read device -- Click 'CTRL+R'

if(!HoldForWindow("Select Device", 100, 10))
	return

Send {Enter}	; Select device -- Click OK

if(!HoldForWindow("Stop Logger?", 100, 10))
	return
	
Send {Enter}	; Stop logging? -- Click 'Don't Stop'


if(!HoldForWindow("Save", 500, 10)) ;; Requires under a second for a few data points but longer for more
	return

Send %filename%	; File dialog

Send {Enter}	; Save file`

Sleep 10

If WinExist("Warning")	; Overvwrite file dialog
	Send {Enter}
	
if(!HoldForWindow("Plot Setup", 100, 10))
	return

Send {Enter}	; Plot setup

;Send ^+WK		; Close window

return