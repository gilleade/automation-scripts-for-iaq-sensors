;; TITLE: HOBOware sensor script
;; DESCRIPTION: Simulates launch/read actions for HOBOware sensor (see readme for setup instructions)
;; AUTHOR: Kiel Gilleade
;; DATE: 8/3/2020

HoldForWindow(dialogTitle, attempts, sleepDuration)
{
	count:= 0
	while(true){
		If WinExist(dialogTitle){
			WinActivate
			return true
		}
		else {
			count++
			Sleep sleepDuration
		}
	
		if (count >= attempts ){
			MsgBox, Failed to load %dialogTitle%
			return false
		}
	}
}

InitHOBO()
{
	If WinExist("HOBOware")
		WinActivate
	else
		run D:\Program Files\Onset Computer Corporation\HOBOware\HOBOware.exe

	if(!HoldForWindow("HOBOware", 200, 10))	;; Requires more than 1 second to boot
		return false
	
	return true
}

Launcher()
{
	Send ^l	; Launch device -- Click 'CTRL+L'

	if(!HoldForWindow("Select Device", 100, 20))
		return false

	Send {Enter}	; Launch device with current settings -- Click OK
					; There is no tab control to configure software
	Sleep 750

	If WinExist("Low Battery")	; 
		Send {Enter}
	
	Sleep 750 
	
	If WinExist("Logger Not Read Out")	; Overvwrite any existing data
		Send {Enter}
	
	if(!HoldForWindow("Launch Logger", 100, 20))
		return false
		
	Send {Enter}
					
	While WinExist("Launching Logger")
		Sleep 100
		
	return true
}

GetData(filename)
{
	Send ^r	; Read device -- Click 'CTRL+R'

	if(!HoldForWindow("Select Device", 100, 10))
		return false

	Send {Enter}	; Select device -- Click OK

		
	if(!HoldForWindow("Stop Logger?", 100, 10))
		return false
		
	Send {Enter}	; Stop logging? -- Click 'Don't Stop'


	if(!HoldForWindow("Save", 500, 10)) ;; Requires under a second for a few data points but longer for more
		return false

	Send %filename%	; File dialog

	Send {Enter}	; Save file`

	Sleep 10

	If WinExist("Warning")	; Overvwrite file dialog
		Send {Enter}
		
	if(!HoldForWindow("Plot Setup", 100, 10))
		return false

	Send {Enter}	; Plot setup
	
	return true
}

^j::
counter:= 0
FormatTime, prefix, ,yyMMdd_hhmmss

while (counter < 5){
	
	if(!InitHOBO())
		return
	if(!Launcher())
		return
	
	Sleep 60000
	
	filename:= prefix . counter

	if(!InitHOBO())
		return
	if(!GetData(filename))
		return

	Sleep 1000
	counter++
}
	
;Send ^+WK		; Close window

return