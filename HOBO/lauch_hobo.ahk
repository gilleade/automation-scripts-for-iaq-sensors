;; TITLE: HOBOware sensor script
;; DESCRIPTION: Simulates launch action for HOBOware sensor (see readme for setup instructions)
;; AUTHOR: Kiel Gilleade
;; DATE: 8/3/2020

HoldForWindow(dialogTitle, attempts, sleepDuration)
{
	count:= 0
	while(true){
		If WinExist(dialogTitle){
			WinActivate
			return true
		}
		else {
			count++
			Sleep sleepDuration
		}
	
		if (count >= attempts ){
			MsgBox, Failed to load %dialogTitle%
			return false
		}
	}
}

^j::
filename:=dummydata

If WinExist("HOBOware")
	WinActivate
else
	run D:\Program Files\Onset Computer Corporation\HOBOware\HOBOware.exe

if(!HoldForWindow("HOBOware", 200, 10))	;; Requires more than 1 second to boot
	return

Send ^l	; Launch device -- Click 'CTRL+L'


if(!HoldForWindow("Select Device", 200, 10))
	return

Send {Enter}	; Launch device with current settings -- Click OK
				; There is no tab control to configure software

Sleep 1000

If WinExist("Low Battery")	; 
	Send {Enter}

Sleep 1000 

If WinExist("Logger Not Read Out")	; Overvwrite any existing data
	Send {Enter}

if(!HoldForWindow("Launch Logger", 200, 10))
	return
	
Send {Enter}
				
While WinExist("Launching Logger")
	Sleep 100
	
;Send ^+WK		; Close window

return