# Automation Scripts for IAQ Sensors

AUTOHOTKEY SCRIPTS

Scripts
- simulate_hobo (runs launch/read sequence 5 times)
- (old code) getdata_hobo (retieves data from previously launched temp/RH sensor) 
- (old code) launch_hobo (launches temp/RH sensor)
- siumulate_easylog (runs launch/read sequence 5 times) 

Requirements
- HOBOware 
- EasyLogUSB
- Autohotkey
- Windows OS

HOBOWARE

Hardware/Software Setup
- Install Hoboware & Autohotkey
- Open HOBOware, File->Preferences, under General set "Export file type" to Excel and tick "Automatically export table data.."
- Connect RH/Temp sensor, launch device with the following deployment options, sampling to 1 min , launch NOW (current script can not set these options).
- Close HOBOware 

Configure Script
- Open script to run in Notepad (or other text editor).
- Find the following section:

	If WinExist("HOBOware")
		WinActivate
	else
		run D:\Program Files\Onset Computer Corporation\HOBOware\HOBOware.exe

- Change the runtime location of HOBOware to match your setup.

How to run
- Right click a Autohotkey script and select run script to load into Autohotkey (an icon will appear in the taskbar)
- Press Ctrl + J on keyboard to run script
- HOBOware will launch and run the selected script
- If a script fails cancel via Authotkey tray icon (right-click and click pause script) 

Notes on running
- Script timings are configured for the development machine
- If a timing is not correct an error will be thrown indicating what dialog was not successfully detected (e.g. "HOBOware not detected" -- software took longer to load than the script is designed to wait for).
- Script timings can be edited in Notepad (or other text editor).
- Dialogs are either core or optional to a script sequence (HoldForWindow is used on a core dialog). 

EasyLogUSB

Hardware/Software Setup
- Install EasyLogUSB & Autohotkey
- Connect Lascar sensor, launch the device with sampling frequencey set to 10 secs (3 day). Everything else leave as default. 
NB. Currently script can't change the sampling frequency.
- Close Lascar
 
Configure Script
- Open script to run in Notepad (or other text editor).
- Find the following section:

	path:= "C:\Program Files (x86)\EasyLog USB\EasyLog USB.exe"
	
- Change the runtime location of EasyLogUSB to match your setup.

How to run
- Right click a Autohotkey script and select run script to load into Autohotkey (an icon will appear in the taskbar)
- Press Ctrl + J on keyboard to run script
- EasyLogUSB will launch and run the selected script 
NB. Script will turn EasyLogUSB off after ever operation (stability reasons).
- If a script fails cancel via Authotkey tray icon (right-click and click pause script) 

Notes on running
- Script times are configured for development machine.
- If a timing is not correct an error will be thrown indicating what dialog was not successfully detected (e.g. "HOBOware not detected" -- software took longer to load than the script is designed to wait for).
- Script timings can be edited in Notepad (or other text editor).
- Dialogs are either core or optional to a script sequence (HoldForWindow is used on a core dialog). 
- EasyLogUSB proved unstable under fast interaction times. 


TODO

Hoboware
- Add battery detection to all scripts (only simulate_hobo has this)
- Add update software dialog detection
- Add configuration detection to launch logger dialog (Window SPY can't identify GUI items to send messages to -- try image detection or mouse pointer offsets) 
- Detect bad write error (rare event prevents launching)
- Identify CSV issue -- sometimes CSV files are not exported, unknown cause (only HOBO file is saved)

EasyLogUSB
- Add support for following error messages:
-- Error Downloading (occured when speed testing script / requires EasyLogUSB shutdown)
-- Error Communicating with logger (occured when speed testing script / requires EasyLogUSB shutdown)
- Could not get ControlSend to reliably target button controls and had to use standard key commands. Need to investigate.
